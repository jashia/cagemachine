namespace :import do
	desc "imports data from JSON"
	task :fighters => :environment do
		require 'json'
		fighters_file = File.read("db/docs_to_import/fighters.json")
		fighters = JSON.parse(fighters_file)
		fighters.each do |fighter|
			first_name = fighter["first_name"]
			nickname = fighter["nickname"]
			last_name = fighter["last_name"]
			legacy_id = fighter["id"]
			legacy_link = fighter["link"]
			legacy_wins = fighter["wins"]
			legacy_losses = fighter["losses"]
			legacy_draws = fighter["draws"]
			weightclass = fighter["weight_class"]
			image_string = fighter["profile_image"]
			remote_image_url = image_string.to_s.chomp("?w600-h600-tc1")

			if Fighter.where("first_name = ? and last_name = ?", first_name, last_name).exists? || last_name == "To be determined" || first_name == ("." || "...") 
				next
			else
				Fighter.create(first_name: first_name, nickname: nickname, last_name: last_name, legacy_id: legacy_id, legacy_link: legacy_link, legacy_wins: legacy_wins, legacy_draws: legacy_draws, legacy_losses: legacy_losses, remote_image_url: remote_image_url, active: false, weightclass: weightclass)
			end
		end
		puts "Fighters Imported"
	end
	task :events => :environment do
		require 'json'
		events_file = File.read("db/docs_to_import/events.json")
		events = JSON.parse(events_file)
		puts events[0].keys
	end
end