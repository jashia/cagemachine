$(document).on('page:change', function() {
// Tracking variables for feature verification. 

 if (window._gaq != null) {
  return _gaq.push(['_trackPageview']);
 } else if (window.pageTracker != null) {
  return pageTracker._trackPageview();
 }
});

var userClicksTrophy = function() {
	ga('send', 'event', 'User Clicks on Trophy', 'Click on button', 'User Clicks on Trophy');
};
var userVisitsLeague = function() {
	new GaEvents.Event('User Visits League', 'Click on button', 'User Visits League');
};

var userVisitsLeague = function(){
	new GaEvents.Event('User Visits League', 'Click on button', 'User Visits League');
};
var userVisitsLeagues = function(){
	new GaEvents.Event('User Finds League', 'Click on button', 'User Finds League');
};
var userClicksUserSettings = function(){
	new GaEvents.Event('button', 'click', 'user settings');
};
var userClicksShowGame = function(){
	new GaEvents.Event('Make Picks', 'click', 'Show Game');
};
var userInvitesFriend = function(){
	new GaEvents.Event('User Invites From League', 'Click on button', 'User Invites Friend via email');
};
var userClicksLeagueChallengesSlider = function(){
	new GaEvents.Event('User clicks challenges slider', 'click', 'Challenge Slider button');
};
var userClicksSeeMoreLeaguePlayers = function(){
	new GaEvents.Event('Leagues', 'click', 'User clicks see more');
};
var userSubmitsPicks = function(){
	new GaEvents.Event('User Submits Picks', 'Click on Button', 'User clicks Submit Picks');
};
var userClicksFighterPictureToSelectFighter = function(){
	new GaEvents.Event('User Selects Fighter', 'Click on choice', 'User Selects Fighter');
};
var userClicksDropDownMenuToSelectFighter = function(){
	new GaEvents.Event('User Scrolls Dropdown', 'Scroll through choices', 'User Scrolls Fighter Choices');
};
var userWillSubmitPicksForPoints = function(){
	new GaEvents.Event('User Shows Picks', 'Click on button', 'User clicks Shows Picks');
};
var userWantsToKnowInfoAboutFighters = function(){
	new GaEvents.Event('Users Asks For Information', 'Click on button', 'User clicks I Button');
};
var userWantsToKnowOdds = function(){
	new GaEvents.Event('User Selects Favorite', 'Click on button', 'Users selects Favorite Fighter');
};
var userSavesPicks = function(){
	new GaEvents.Event('User saves picks', 'Click on button', 'User Saves Picks');
};
var userFindsAndVisitsLeague = function(){
	new GaEvents.Event('User Visits League','Click on button', 'User Visits League');
};
var userJoinsLeague = function(){
	new GaEvents.Event('User Joins League', 'Click on button', 'User Joins League');
};
var userWantsToBeChampAndVisitsChamp = function(){
	new GaEvents.Event('Users Visits Champion','Click on button', 'Users Visits Champion Page');
};
var userWillEditLeague = function(){
	new GaEvents.Event('User Edits League', 'Clicks on button', 'User Edits League'); 
};