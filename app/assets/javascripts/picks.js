var picks = function() {
	
	$('#picks-sidebar li').on('click', function() {
		var current = $('li.active');
		current.removeClass('active');
		$(this).addClass('active');
	});	

	var clickEvent = false;
	$('#picks').on('slid.bs.carousel', function(e) {
		if(!clickEvent) {
			var count = $('#picks-sidebar-content ul').children().length -1;
			var current = $('#picks-sidebar-content ul li.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if(count == id) {
				$('#picks-sidebar-content ul li').first().addClass('active');	
			}
		}
		clickEvent = false;
	});

	$('#picks').on('slid.bs.carousel', function() {
		$('.carousel-inner .item').each( function( index, element ) {
			var current = $('.carousel-inner .item.active');
			var itemIndex = current.index();
			$('#picks-sidebar-content ul li.active').removeClass("active");		
			$('#picks-sidebar-content ul li').eq(itemIndex).addClass("active");
			console.log(itemIndex);
		});
	});

};


var picksComplete = function() {
	var count = 0;
	function checkCurrentGame() {
		$('.carousel-inner .item').each( function( index, element ) {
			var itemIndex = $( this ).index();
			var itemInput = $( this ).find("input");
			if (itemInput.is(':checked')) {
				$('#picks-sidebar-content ul li').eq(itemIndex).addClass("checked");
				count += 5;
			}
			else {
				$('#picks-sidebar-content ul li').eq(itemIndex).addClass("unchecked");
			}
		});
	};

	function updateCurrentGame() {
		count = count;
		var pickReady;
		var itemIndex;
		var pickedFighter, pickedRound, pickedMethod, pickedMethodDecision;
		var checkPickReady = function () {
			var addPoints = function () {
				var checkedListItems = $('#picks-sidebar-content ul li.checked');
				var pointTotal = checkedListItems.length * 5;
				$('#picks-sidebar-content p.points').text("Total Possible Points: " + ( pointTotal ));
			};
				$('#picks-sidebar-content ul li').eq(itemIndex).addClass("checked");
				$('#picks-sidebar-content ul li').eq(itemIndex).removeClass("unchecked");
				addPoints();
		};

		var yupPickReady = function () {

		}

		$('.pick-fighter label').on( "click", function() {
			var starter = $(this);
			closestItem = $(this).closest(".item");
			itemIndex = closestItem.index();
			var pickedRound = starter.closest(".pick").find(".pick-rounds input[type=radio]");
			var pickedMethod = starter.closest(".pick").find(".pick-methods input[type=radio]");
			var pickedMethodDecision = starter.closest(".pick").find(".pick-methods input[type=radio].method-decision").is(":checked");

			if ( (pickedRound.is(":checked") == true) && (pickedMethod.is(":checked") == true ) ) {
				checkPickReady();
				console.log("Picked fighter last");
			} else if (pickedMethodDecision == true) { 
				checkPickReady();
				console.log("Picked a DECISION and a fighter last");
			};
			// pickedFighter = true;
			// checkPickReady();

		});
		$('.pick-rounds label').on( "click", function() {
			var starter = $(this);
			closestItem = $(this).closest(".item");
			itemIndex = closestItem.index();
			var pickedMethod = starter.closest(".pick").find(".pick-methods input[type=radio]").is(':checked');
			var pickedMethodDecision = starter.closest(".pick").find(".pick-methods input[type=radio].method-decision").is(":checked");
			var pickedFighter = starter.closest('.pick').find(".pick-fighters input[type=radio]").is(':checked');

			if (pickedFighter == true && pickedMethod == true) {
				checkPickReady();
			} 
		});

		$('.pick-methods label').on("click", function() {
			var starter = $(this);
			closestItem = $(this).closest(".item");
			itemIndex = closestItem.index();
			var pickedFighter = starter.closest('.pick').find(".pick-fighters input[type=radio]").is(':checked');
			var pickedRound = starter.closest(".pick").find(".pick-rounds input[type=radio]").is(':checked');
			var pickedMethodDecision = starter.closest(".pick").find(".pick-methods input[type=radio].method-decision").is(":checked");

			if (pickedRound == true && pickedFighter == true) {
				checkPickReady();
			} else if ( starter.text() == "DECISION" && pickedFighter == true ) {
				checkPickReady();
			} else if ( ( starter.text() == "SUBMISSION" || starter.text() == "KO/TKO") && (pickedRound == false) ) {
				$('#picks-sidebar-content ul li').eq(itemIndex).addClass("unchecked");
				$('#picks-sidebar-content ul li').eq(itemIndex).removeClass("checked");
			}

		});

		$('.carousel-inner .item').each( function( index, element ) {
				if ($('.carousel-inner .item.active').index() == 0) {
					$('a.picks-control.left').hide();
				};
				$('#picks').on('slid.bs.carousel', function () {
					var picksLength = $('.item').length;
					if ($( '.carousel-inner .item.active' ).index() === picksLength - 1 ) {
						$('a.picks-control.left').show();
						$('a.picks-control.right').hide();
					} else if ( $( '.carousel-inner .item.active' ).index() === 0 ) {
						$('a.picks-control.left').hide();
						$('a.picks-control.right').show();
					} else {
						$('a.picks-control.left').show();
						$('a.picks-control.right').show();
					}
    			});
		});
	};

	checkCurrentGame();
	updateCurrentGame();
	
	
	$('#picks-sidebar-content p.points').append(count);
};
$(document).ready(picks);
$(document).ready(picksComplete);
// $(document).on("page:load ready", picksComplete);
// $(document).on("page:load ready", picks);
