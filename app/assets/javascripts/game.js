var decision = function() {
	$('.method-decision').click(function()
	{
		$(this).parent().parent().parent().find('input.rounds').prop("disabled", "disabled");
		$(this).parent().parent().parent().find('input.rounds').removeAttr("checked");
		$(this).parent().parent().parent().find('input.rounds').next("label").css("text-decoration", "line-through");
	});

	$('.method').click(function() {
		$(this).parent().parent().parent().find('input.rounds').removeAttr("disabled");
		$(this).parent().parent().parent().find('input.rounds').next("label").css("text-decoration", "none");
	});

	
}
$(document).ready(decision);
//$(document).on("page:load ready", decision);