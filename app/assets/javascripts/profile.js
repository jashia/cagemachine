// $(document).ready(function(){
//   setTimeout(function(){
//     $('.alert').fadeOut();
//   }, 7000);
// });

var helpButton = function() {
	$('i.tips').hide();
	$('a.hide-help').hide();
	$('a.help-please').on("click", function() {
		$('i.tips').show();
		$('a.hide-help').show();
		$('a.help-please').hide();
	})
	$('a.hide-help').on("click", function() {
		$('i.tips').hide();
		$('a.hide-help').hide();
		$('a.help-please').show();
	})
};

$(document).ready(helpButton);