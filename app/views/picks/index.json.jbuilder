json.array!(@picks) do |pick|
  json.extract! pick, :id, :fight_id, :user_id, :winner, :method, :round
  json.url pick_url(pick, format: :json)
end
