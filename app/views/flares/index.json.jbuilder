json.array!(@flares) do |flare|
  json.extract! flare, :id, :title, :description, :image, :premium
  json.url flare_url(flare, format: :json)
end
