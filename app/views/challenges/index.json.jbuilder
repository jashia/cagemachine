json.array!(@challenges) do |challenge|
  json.extract! challenge, :id, :fight_id, :stage, :challenger_id, :challenged_id
  json.url challenge_url(challenge, format: :json)
end
