json.array!(@fights) do |fight|
  json.extract! fight, :id, :event_id, :fighter_1_id, :fighter_1_odds, :fighter_2_id, :fighter_2_odds, :order, :bout_type, :five_rounds, :championship_bout, :winner, :method, :round
  json.url fight_url(fight, format: :json)
end
