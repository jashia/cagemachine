json.array!(@game_states) do |game_state|
  json.extract! game_state, :id, :user_id, :event_id, :picks_locked, :roster_locked
  json.url game_state_url(game_state, format: :json)
end
