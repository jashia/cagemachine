json.array!(@points) do |point|
  json.extract! point, :id, :user_id, :points, :money, :challenges
  json.url point_url(point, format: :json)
end
