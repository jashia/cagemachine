json.array!(@events) do |event|
  json.extract! event, :id, :title, :location, :arena, :pay_per_view, :active, :start_time, :fighter_1, :fighter_2
  json.url event_url(event, format: :json)
end
