json.array!(@leagues) do |league|
  json.extract! league, :id, :name, :season,
  json.url league_url(league, format: :json)
end
