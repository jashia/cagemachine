json.array!(@bar_tabs) do |bar_tab|
  json.extract! bar_tab, :id, :user_id, :item, :table, :status, :delivered
  json.url bar_tab_url(bar_tab, format: :json)
end
