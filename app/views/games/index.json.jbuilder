json.array!(@games) do |game|
  json.extract! game, :id, :user_id, :event_id, :game_type, :score
  json.url game_url(game, format: :json)
end
