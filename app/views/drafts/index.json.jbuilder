json.array!(@drafts) do |draft|
  json.extract! draft, :id, :event, :user_id, :fighter_1_id, :fighter_2_id, :fighter_3_id
  json.url draft_url(draft, format: :json)
end
