class Pick < ActiveRecord::Base
  belongs_to :fight
  belongs_to :user
  belongs_to :game
  #validates_presence_of :winner

  def score
  	call = 0
    case 
  		when winner == fight.winner && method == "DECISION" && method == fight.method then call = 2
    	when winner == nil then call = 0
    	when winner == fight.winner && method == fight.method && round == fight.round && round != nil then call = 5
    	when winner == fight.winner && method == fight.method || winner == fight.winner && round == fight.round then call = 3
    	when winner == fight.winner then call = 1
    	when fight.winner != "" && winner != fight.winner then call = 0
    end

    if fight.championship_bout == true
      call = call * 5
    elsif fight.bout_type == "Main Event"
      call = call * 4
    elsif fight.bout_type == "Co-Main Event"
      call = call * 3
    elsif fight.bout_type == "Main Card"
      call = call * 2
    else
      call
    end

    unless call == 0
      unless self.fight.challenges.where("challenger_id = ? OR challenged_id = ?", user, user) == []
        self.fight.challenges.where("challenger_id = ? OR challenged_id = ?", user, user).each do |challenge|
          if challenge.stage == "Challenge Accepted"
            call = call + 20
          elsif challenge.stage == "Double Down Accepted"
            call = call + 40
          end
        end
      end
    end
    call
  end

  def in_plain_english
    fighter_1 = self.fight.fighter_1.full_name
    fighter_2 = self.fight.fighter_2.full_name
    unless winner == nil
      if winner == fighter_1
         your_pick = "#{fighter_1} defeats #{fighter_2}"
      else
         your_pick = "#{fighter_2} defeats #{fighter_1}"
      end
    else 
      your_pick = "Please select a WINNER"
    end      
    your_pick
  end

  def score_color

    if fight.winner.blank?
      ""
    elsif score == 0
      "-incorrect"
    else
      "-correct"
    end
  end

  def decision_indicator
    if self != []
      if method == "DECISION"
        "text-decoration: line-through"   
      end
    end
  end
end
