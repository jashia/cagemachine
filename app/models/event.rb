class Event < ActiveRecord::Base
	has_many :fights, dependent: :destroy
	has_many :games, dependent: :destroy
    belongs_to :fighter_1, class_name: "Fighter"
	belongs_to :fighter_2, class_name: "Fighter"
	has_many :users, through: :games
	belongs_to :season
	accepts_nested_attributes_for :fights

	def score(user)
		if games.where(user_id: user.id) != []
			self.games.find_by(user_id: user.id).picks.to_a.sum(&:score)
		else
			"No game yet"
		end
	end
end