class Roster < ActiveRecord::Base
	belongs_to :membership
	has_many :drafts
	has_many :fighters, through: :drafts
	accepts_nested_attributes_for :drafts
end
