class Fight < ActiveRecord::Base
  belongs_to :event
  has_many :picks, dependent: :destroy
  belongs_to :fighter_1, class_name: "Fighter"
  belongs_to :fighter_2, class_name: "Fighter"
  has_many :challenges, dependent: :destroy


  def fighters
  	[fighter_1, fighter_2]
  end

  def bout
  	"#{fighter_1.display_name} vs. #{fighter_2.display_name}"
  end

  def name 
    self.bout    
  end

  def add_points
    picks.each do |pick|
      user = pick.user
      user.points = user.points + pick.score
      user.coins = user.coins + pick.score
      user.save!
    end
    challenges.each do |c|
      pick = self.picks.where(user_id: c.challenged).take
      if pick.winner == self.winner 
        c.winner = c.challenged.username
        c.save
        if c.beer == true && c.stage == "Double Down Accepted"
          2.times do 
            tab = c.bar_tabs.new
            tab.item = "beer"
            tab.status = "open"
            tab.save
          end
        elsif c.beer == true && c.stage == "Challenge Accepted"
            tab = c.bar_tabs.new
            tab.item = "beer"
            tab.status = "open"
            tab.save
        else
          if c.stage == "Double Down Accepted"
            c.challenged.coins += 40
            c.challenged.points += 40
            c.challenged.save!
          elsif c.stage == "Challenge Accepted"
            c.challenged.coins += 20
            c.challenged.points += 20
            c.challenged.save!
          end
        end
      else
        c.winner = c.challenger.username
        c.save
        if c.beer == true && c.stage == "Double Down Accepted"
          2.times do 
            tab = c.bar_tabs.new
            tab.item = "beer"
            tab.status = "open"
            tab.save
          end
        elsif c.beer == true && c.stage == "Challenge Accepted"
            tab = c.bar_tabs.new
            tab.item = "beer"
            tab.status = "open"
            tab.save
        else
          if c.stage == "Double Down Accepted"
            c.challenged.coins += 40
            c.challenged.points += 40
            c.challenged.save!
          elsif c.stage == "Challenge Accepted"
            c.challenged.coins += 20
            c.challenged.points += 20
            c.challenged.save!
          end
        end
      end
    end
  end


  def your_winner(user)
    picks.find_by(user_id: user.id)
  end
end
