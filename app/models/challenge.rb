class Challenge < ActiveRecord::Base
  belongs_to :fight
  belongs_to :challenger, class_name: "User"
  belongs_to :challenged, class_name: "User"
  before_create :make_sure_challenger_coins_available
  before_update :make_sure_challenged_coins_available
  validate :make_sure_fight_unlocked
  before_update :make_sure_fight_unlocked
  has_many :bar_tabs, dependent: :destroy

  def subtract_coins_from_challenger
  	challenger.coins -= 10
  	challenger.save
  end

  def coin_escrow
  	if stage == "Challenge Accepted"
  		challenged.coins -= 10
  		challenged.save
  	elsif stage == "Double Down"
  		challenged.coins -= 20
  		challenged.save
  	elsif stage == "Challenge Rejected"
  		challenger.coins += 10
  		challenger.save
    elsif stage == "Double Down Rejected"
      challenged.coins += 20
      challenged.save
    elsif stage == "Double Down Accepted"
      challenger.coins -= 10
      challenger.save
  	end
  end

  protected
    def make_sure_challenger_coins_available
      challenger.coins >= 10      
    end

    def make_sure_challenged_coins_available
      if stage == "Challenge Accepted"
        challenged.coins >= 10
      elsif stage == "Double Down Accepted"
        challenger.coins >= 10
      elsif stage == "Double Down Issued"
        challenged.coins >= 20
      elsif stage == "Challenge Rejected" || stage == "Double Down Rejected"
        challenged.coins >= 0 || challenger.coins >= 0
      end      
    end

    def make_sure_fight_unlocked
      if fight.lock == true
        errors.add(:base, "Sorry, this fight is locked.")
        false
      else
        true
      end

    end



#  def score
#    if fight.picks.where('user_id = challenger.id').score > 0
#    fight.where()
#    
#  end

  
end
