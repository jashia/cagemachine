class Fighter < ActiveRecord::Base
	mount_uploader :image, ImageUploader 
	has_many :fights_as_fighter_1, class_name: "Fight", foreign_key: "fighter_1_id"
	has_many :fights_as_fighter_2, class_name: "Fight", foreign_key: "fighter_2_id"
	has_many :events_as_fighter_1, class_name: "Event", foreign_key: "fighter_1_id"
	has_many :events_as_fighter_2, class_name: "Event", foreign_key: "fighter_2_id"
	has_many :drafts, dependent: :destroy
	has_many :rosters, through: :drafts
	validates_presence_of :first_name
	has_many :users

	def display_name
		"#{first_name} #{last_name}"
	end


	def full_name
		"#{first_name}" + " " + "#{last_name}"
	end

	def image_blank
		if image == nil
			true
		end
	end

	def heavyweights
		Fighter.where(weightclass: "Heavyweight")		
	end
end
