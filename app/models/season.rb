class Season < ActiveRecord::Base
	has_many :events
	has_many :leagues
end
