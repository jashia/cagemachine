class Membership < ActiveRecord::Base
  belongs_to :user
  belongs_to :league
  has_one :roster, dependent: :destroy
  validate :on => :create do
    if user.memberships.count >= 2
      errors[:base] << "too many things"
    end
  end
  def add_points_for_joining
  end

  def subtract_points_for_leaving

  end

  def event_points(user)
     if Event.where(active: true).first.games.where(user_id: user.id) != []
       gamepoints = Event.where(active: true).first.games.where(user_id: user.id).first.picks.to_a.sum(&:score)   
     else
       gamepoints = 0
     end
  end

  def event_points_sort
     if Event.where(active: true).first.games.where(user_id: user.id) != []
  	   gamepoints = Event.where(active: true).first.games.where(user_id: user.id).first.picks.to_a.sum(&:score)   
  	 else
  		 gamepoints = 0
  	 end
  end

  def add_event_points_to_user
  	if Event.where(active: true).first.games.where(user_id: user.id).exists?
  	 gamepoints = Event.where(active: true).first.games.where(user_id: user.id).first.picks.to_a.sum(&:score)
	
  	 all_points = points + gamepoints
  	user.points = user.points + all_points 
  	else
  		user.points = points + user.points
  	end
  	user.save
  end

  def season_points_sort
    season_points = 0
    membership = self
    if league.kind == "Fight Night"
      latest_event = Event.order(:start_time).find_by(active: true)
      latest_event.games.where(user_id: membership.user.id).each do |game|
        game_score = game.picks.to_a.sum(&:score)
        season_points += game_score
      end
    elsif league.kind == "Season"
      latest_season = Season.order(:start).find_by(active: true)
      latest_season.events.each do |event|
        event.games.where(user_id: membership.user.id).each do |game|
          game_score = game.picks.to_a.sum(&:score)
          season_points += game_score 
        end
      end
    end
    season_points
  end

  def season_points(membership)
    season_points = 0
    if league.kind == "Fight Night"
      latest_event = Event.order(:start_time).find_by(active: true)
      latest_event.games.where(user_id: membership.user.id).each do |game|
        game_score = game.picks.to_a.sum(&:score)
        season_points += game_score
      end
    elsif league.kind == "Season"
      latest_season = Season.order(:start).find_by(active: true)
      latest_season.events.each do |event|
        event.games.where(user_id: membership.user.id).each do |game|
          game_score = game.picks.to_a.sum(&:score)
          season_points += game_score 
        end
      end
    end
   season_points 
  end

  def points_till_champion(user, league)
    member = league.memberships.find_by(user_id: user.id)
    if member == self
      "You are the Champion!"
    else
      points_to_go = season_points(self) - season_points(member) + 1
      "You still need " + points_to_go.to_s + " " + "points to be the champion." 
    end
  end

end
