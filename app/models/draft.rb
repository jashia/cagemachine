class Draft < ActiveRecord::Base
  belongs_to :user
  belongs_to :fighter
  belongs_to :roster
  before_save :ensure_fighter_is_available, message: "Fighter has been taken"

  protected

  	def ensure_fighter_is_available
  		selected_fighter = self.fighter
  		league = self.roster.membership.league
  		league.memberships.each do |m|
  			m.rosters.each do |r|
  				r.drafts.each do |d|
  					if d.fighter = selected_fighter
  						false
  					end
  				end
  			end
  		end
  	end
end
