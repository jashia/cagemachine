class Game < ActiveRecord::Base
  belongs_to :user
  belongs_to :event
  has_many :picks, dependent: :destroy
  accepts_nested_attributes_for :picks
  before_create :make_sure_event_unlocked
  before_update :make_sure_event_unlocked

  def add_points_for_new_game
  	user.points = user.points + 50
    user.coins = user.coins + 50
    user.save
  end

  def subtract_points_for_destroyed_game
  	user.points -= 50
    user.coins -= 50
  	user.save
  end

  protected
    def make_sure_event_unlocked
      event.lock != true
    end

end
