class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :terms, presence: true
  has_many :memberships, dependent: :destroy
  has_many :leagues, through: :memberships
  has_many :invitations, :class_name => "Invite", :foreign_key => 'recipient_id'
  has_many :sent_invites, :class_name => "Invite", :foreign_key => 'sender_id'

  has_one :league, class_name: "League", :foreign_key => 'owner_id'
  has_many :games, dependent: :destroy
  has_many :events, through: :games
  has_many :challenges_as_challenger, class_name: "Challenge", :foreign_key => "challenger_id"
  has_many :challenges_as_challenged, class_name: "Challenge", :foreign_key => "challenged_id"
  validates :coins, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  belongs_to :fighter


  def to_param
    username
  end
  def join(league)
  	memberships.create(league_id: league)
  end

  def leave(league)
  	memberships.find_by(user_id: current_user.id, league_id: league.id).destroy  	
  end

  def joined?(league)
  	memberships.include?(user_id: current_user.id, league_id: league.id)
  end

  def total_points
    self.games.each do |game|
      @w = game.picks.to_a.sum(&:score)
    end
    self.points + @w.to_i
  end

  def user_level
    case
      when points <= 100 then "White Belt"
      when points > 100 && points <= 500 then "Yellow Belt"
      when points > 500 && points <= 900 then "Green Belt"
      when points > 900 && points <= 1500 then "Brown Belt"
      else "Black Belt"
    end
  end

  def badges
    badges = []
    leagues.each do |league|
      league.memberships.sort_by(&:season_points_sort).reverse[0..3].each.with_index do |member, index|
        if member.user == self
          case
            when index == 0 then badges << ["Champ", league]
            when index == 1 then badges << ["1st", league]
            when index == 2 then badges << ["2nd", league]
            when index == 3 then badges << ["3rd", league]
          end
        end
      end
    end
    badges
  end

  def full_name
    unless (first_name == nil) || (last_name == nil)
      "#{first_name}" + " " + "#{last_name}"
    else
      "Your Friend"      
    end
  end

end


