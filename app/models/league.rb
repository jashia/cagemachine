class League < ActiveRecord::Base
  belongs_to :commissioner
  has_many :memberships, dependent: :destroy
  has_many :users, through: :memberships
  accepts_nested_attributes_for :memberships
  has_many :invites
  belongs_to :owner, class_name: "User"
  belongs_to :flare
  belongs_to :season


  def owned_by(user)
  	if user == self.owner
  		"You are the league owner"
  	end
  end

  def season_name
    if kind == "Fight Night"
      Event.order(:start_time).find_by(active: true).title
    elsif kind == "Season"
      Season.order(:start).find_by(active: true).name
    else
      "No Season"
    end   
  end


  def season_start
    if kind == "Fight Night"
      Event.order(:start_time).find_by(active: true).start_time.strftime("%A, %B %d, %Y")
    elsif kind == "Season"
      Season.order(:start).find_by(active: true).start.strftime("%A, %B %d, %Y")
    end    
  end
  
  def season_end
    if kind == "Fight Night"
      "At close of event."
    elsif kind == "Season"
      Season.order(:start).find_by(active: true).end.strftime("%A, %B %d, %Y")
    end    
  end  
end
