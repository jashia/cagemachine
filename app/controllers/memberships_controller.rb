class MembershipsController < ApplicationController

    def create
      @membership = Membership.create(membership_params)

      respond_to do |format|
        if @membership.save
          format.html { redirect_to leagues_url, notice: 'You joined that league.' }
          format.json { render :show, status: :created, location: @membership }
        else
          format.html { redirect_to leagues_url, notice: 'You did not join that league. You can only join a maximum of 2 leagues.' }
          format.json { render json: @membership.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      @membership = Membership.find_by(membership_params)
      @membership.subtract_points_for_leaving
      @membership.destroy
      respond_to do |format|
        format.html { redirect_to leagues_url, notice: 'You left that league.' }
        format.json { head :no_content }
      end      
    end
  private
    def membership_params
      params.require(:membership).permit(:id, :league_id, :user_id)
    end


end
