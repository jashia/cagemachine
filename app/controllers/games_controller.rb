class GamesController < ApplicationController
  before_action :set_game, only: [:show, :edit, :submit_picks, :update, :destroy]
  before_filter :authenticate_user!
  before_filter :verify_is_unlocked, only: [:edit, :update]


  # GET /games
  # GET /games.json
  def index
    @games = Game.all
  end

  # GET /games/1
  # GET /games/1.json
  def show
  end

  # GET /games/new
  def new
    @game = Game.new
    # @event = Event.all.order(:start_time).find_by(active: true)
    @event = Event.find(params[:event_id])
  end

  # GET /games/1/edit
  def edit
    @event = @game.event
  end

  # POST /games
  # POST /games.json
  def create
    @game = Game.new(game_params)
    @event = Event.all.order(start_time: :desc).find_by(active: true)    

    respond_to do |format|
      if @game.save
        format.html { redirect_to @game, notice: 'Game was successfully created.' }
        format.json { render :show, status: :created, location: @game }
        @game.add_points_for_new_game

      else
        format.html { render :new }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def update
    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to @game, notice: 'Game was successfully saved.' }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def submit_picks
    @game.lock = true

    respond_to do |format|
      if @game.update(game_params)
        @game.add_points_for_submitting_picks
        format.html { redirect_to @game, notice: 'Game was successfully submitted. Challenges are available. Coins added to your account.' }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @game.subtract_points_for_destroyed_game
    @game.destroy

    respond_to do |format|
      format.html { redirect_to games_url, notice: 'Game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
      @game = Game.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
      params.require(:game).permit(:user_id, :event_id, :game_type, :score, :lock, :picks_attributes => [:id, :fight_id, :user_id, :winner, :method, :round])
    end
    def verify_is_unlocked
      unless current_user.admin?
        if @game.lock == true 
          redirect_to(game_path)
        end 
      end
    end


end
