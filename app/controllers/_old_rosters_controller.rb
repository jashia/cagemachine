class RostersController < InheritedResources::Base

  private

    def roster_params
      params.require(:roster).permit(:belongs_to, :status, :locked)
    end
end

