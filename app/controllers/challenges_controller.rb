class ChallengesController < ApplicationController
  before_action :set_challenge, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  # GET /challenges
  # GET /challenges.json
  def index
    @challenges = Challenge.all
  end

  # GET /challenges/1
  # GET /challenges/1.json
  def show
  end

  # GET /challenges/new
  def new
    @challenge = Challenge.new
  end

  # GET /challenges/1/edit
  def edit
  end

  # POST /challenges
  # POST /challenges.json
  def create
    @challenge = Challenge.new(challenge_params)

    respond_to do |format|
      if @challenge.save
        channel_name = "challenges-#{@challenge.challenged_id}"
        channel = Pusher.channel_info(channel_name)
        if channel[:occupied]
          Pusher.trigger(channel_name, 'challenge_issued', {
            message: "Your pick for the #{@challenge.fight.bout} has been challenged by #{@challenge.challenger.username}. Visit your profile to Accept, Tapout, or Challenge them back."
          })
        else
          ChallengeMailer.new_challenge(@challenge).deliver
        end
        @challenge.subtract_coins_from_challenger
        format.html { redirect_to :back, notice: 'Challenge was successfully created. 10 coins used.' }
        format.json { render :show, status: :created, location: @challenge }

      else
        if @challenge.fight.lock == true
          format.html { redirect_to :back, :flash => { :error => @challenge.errors.full_messages.join(', ') } }
          format.json { render json: @challenge.errors, status: :unprocessable_entity }
        end
          format.html { redirect_to :back, notice: 'Challenge was not issued.' }
          format.json { render json: @challenge.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /challenges/1
  # PATCH/PUT /challenges/1.json
  def update
    respond_to do |format|
      if @challenge.update(challenge_params)
        @challenge.coin_escrow
        if @challenge.stage == "Double Down Issued"
          channel_name = "challenges-#{@challenge.challenger_id}"
          channel = Pusher.channel_info(channel_name)
          if channel[:occupied]
            Pusher.trigger(channel_name, 'challenge_issued', {
              message: "Your challenge for the #{@challenge.fight.bout} has been doubled downed by #{@challenge.challenged.username}. Visit your profile to Accept or Tapout."
            })
          else
            ChallengeMailer.existing_challenge(@challenge).deliver
          end
        end
        format.html { redirect_to :back, notice: 'Challenge was successfully updated.' }
        format.json { render :show, status: :ok, location: @challenge }
      else
        format.html { redirect_to :back, notice: 'Challenge was not accepted. You need more coins.' }
        format.json { render json: @challenge.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /challenges/1
  # DELETE /challenges/1.json
  def destroy
    @challenge.destroy
    respond_to do |format|
      format.html { redirect_to challenges_url, notice: 'Challenge was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_challenge
      @challenge = Challenge.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def challenge_params
      params.require(:challenge).permit(:fight_id, :stage, :challenger_id, :challenged_id, :beer)
    end
end
