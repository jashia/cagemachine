class UsersController < ApplicationController
  before_filter :authenticate_user!

  def index
    @users = User.all
  end

  def new
  end

  def create
  end


  def show
    @user = User.find(params[:id])
    @challenge = Challenge.new
    @events = Event.where(active: true).order(:start_time)
    @nextevent = Event.where(active: true).order(:start_time)
    @badges = @user.badges if signed_in?
  
  end

  def edit
  end

  def destroy
  end

end
