class StaticPagesController < ApplicationController
  def home
    if signed_in?
  	 @leagues = League.includes(:memberships, :season)
  	 @user = User.includes(leagues: [:flare, :memberships]).find(current_user.id)
  	 @events = Event.where(active: true).order(:start_time).includes({fights: :challenges}, :games)
  	 @nextevent = Event.where(active: true).order(:start_time)
     @badges = @user.badges
    end
  end

  def about
  end

  def terms
    @user = current_user    
  end

  def help
  end

  def rules
    
  end
end
