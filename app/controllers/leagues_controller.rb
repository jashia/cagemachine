class LeaguesController < ApplicationController
  before_action :set_league, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  before_filter :verify_is_admin_or_owner, only: [:edit]

  # GET /leagues
  # GET /leagues.json
  def index
    @leagues = League.all
    @memberships = Membership.all
    # @openleagues = League.where(open: true).joins(:memberships).group("memberships.league_id").order("count(memberships.league_id) desc") use this for sorting later on when you learn more about Postgres.
    @openleagues = League.where(open: true)
    @myleagues = current_user.leagues
    @membership = Membership.new
  end

  # GET /leagues/1
  # GET /leagues/1.json
  def show
    @invite = Invite.new
    @user = current_user
    @membership = Membership.new
    @membership_exist = Membership.find_by(league_id: @league.id, user_id: current_user.id)
    @event = Event.where(active: true).order(:start_time).first
    @events = Event.all
    @my_games = Game.all.where(user_id: current_user.id)
    @challenges = Challenge.all 
    @challenge = Challenge.new

  end

  # GET /leagues/new
  def new
    @league = League.new
    @latest_event = Event.order(:start_time).find_by(active: true)
    @latest_season = Season.order(:start).find_by(active: true)

  end

  # GET /leagues/1/edit
  def edit
    @latest_event = Event.order(:start_time).find_by(active: true)
    @latest_season = Season.order(:start).find_by(active: true)    
  end

  # POST /leagues
  # POST /leagues.json
  def create
    @league = League.new(league_params)

    respond_to do |format|
      if @league.save
        @league.memberships.create(user_id: current_user.id)

        format.html { redirect_to @league, notice: 'League was successfully created.' }
        format.json { render :show, status: :created, location: @league }
      else
        format.html { render :new }
        format.json { render json: @league.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /leagues/1
  # PATCH/PUT /leagues/1.json
  def update
    respond_to do |format|
      if @league.update(league_params)
        format.html { redirect_to @league, notice: 'League was successfully updated.' }
        format.json { render :show, status: :ok, location: @league }
      else
        format.html { render :edit }
        format.json { render json: @league.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /leagues/1
  # DELETE /leagues/1.json
  def destroy
    @league.destroy
    respond_to do |format|
      format.html { redirect_to leagues_url, notice: 'League was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_league
      @league = League.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def league_params
      params.require(:league).permit(:name, :season_id, :flare, :open, :owner_id, :flare_id, :kind, memberships_attributes: [:id, :email, :commissioner])
    end

    def verify_is_admin_or_owner
      (current_user.nil?) ? redirect_to(root_path) : (redirect_to(root_path) unless current_user.admin? || (current_user == @league.owner))
    end
end
