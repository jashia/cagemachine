class Users::RegistrationsController < Devise::RegistrationsController
before_filter :configure_sign_up_params, only: [:new, :create]
# before_filter :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    @token = params[:invite_token]
    super
  end

  # POST /resource
  def create
    @newUser = build_resource(sign_up_params)
    @newUser.save
    @token = params[:invite_token]

    if @token != nil
     org = Invite.find_by_token(@token).league #find the organization attached to the invite
     @newUser.leagues.push(org) #add this user to the new organization as a member
     membership = @newUser.memberships.where(league_id: org.id).take
     membership.points = membership.points + 50
     membership.save
    end
    unless Invite.find_by_token(@token) == nil
      referral = Invite.find_by_token(@token)
      if referral.recipient_id != nil
        referral.recipient_id = @newUser.id
        referral.save
      end
    end

    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
        UserMailer.new_user(resource).deliver_now
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
  end

  # GET /resource/edit
  def edit
     super
  end

  # PUT /resource
  def update
    super
  end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.for(:sign_up) << [:invite_token]
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.for(:account_update) << :flare
  end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
