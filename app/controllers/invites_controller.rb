class InvitesController < ApplicationController
  before_action :set_invite, only: [:show, :edit, :update, :destroy]

  # GET /invites
  # GET /invites.json
  def index
    @invites = Invite.all
  end

  # GET /invites/1
  # GET /invites/1.json
  def show
  end

  # GET /invites/new
  def new
    @invite = Invite.new
  end

  # GET /invites/1/edit
  def edit
  end

  # POST /invites
  # POST /invites.json
  def create
    @invite = Invite.new(invite_params)
    @invite.sender_id = current_user.id

    respond_to do |format|
      if @invite.save

      #if the user already exists
        if @invite.recipient != nil 
    
           #send a notification email
           InviteMailer.existing_user_invite(@invite).deliver 
    
           #Add the user to the organization
           @invite.recipient.leagues.push(@invite.league)
           flash[:notice] = 'This player already has an account. They have been added to this league.'

        else
           InviteMailer.new_user_invite(@invite, new_user_registration_url(:invite_token => @invite.token)).deliver
           user = User.find(@invite.sender_id)
           user.coins += 5
           user.save
           flash[:notice] = 'Invitation has been sent. 5 Coins have been added to your account'

        end
          format.html { redirect_to league_path(@invite.league_id) }
          format.json { render :show, status: :created, location: @invite }
      else
        format.html { render :back, notice: 'Invite not sent. Player has an account and has reached their league quota, or some other error.' }
        format.json { render json: @invite.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /invites/1
  # PATCH/PUT /invites/1.json
  def update
    respond_to do |format|
      if @invite.update(invite_params)
        format.html { redirect_to @invite, notice: 'Invite was successfully updated.' }
        format.json { render :show, status: :ok, location: @invite }
      else
        format.html { render :edit }
        format.json { render json: @invite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invites/1
  # DELETE /invites/1.json
  def destroy
    @invite.destroy
    respond_to do |format|
      format.html { redirect_to invites_url, notice: 'Invite was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invite
      @invite = Invite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invite_params
      params.require(:invite).permit(:email, :sender_id, :league_id, :recipient_id)
    end
end
