class BarTabsController < InheritedResources::Base

  private

    def bar_tab_params
      params.require(:bar_tab).permit(:challenge_id, :item, :table, :status, :delivered)
    end
end

