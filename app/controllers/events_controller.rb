class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  before_filter :verify_is_admin
  add_flash_types :event_notifications
  
  # GET /events
  # GET /events.json
  def index
    @events = Event.all
  end

  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update  
    respond_to do |format|
      if @event.update(event_params)    
        flash[:notice] = 'Event was updated Yo'
        if @event.previous_changes['active'] != [] && @event.previous_changes['active'] == [false, true]
          flash[:event_notifications] = 'Event is now active.'
          @users = User.all
          @users.each do |u|
            EventMailer.new_active_event(@event, u).deliver
          end
        elsif @event.previous_changes['lock'] != nil && @event.previous_changes['lock'] != [true, false]
           flash[:event_notifications] = 'Event is now locked.'
           EventMailer.event_locked(@event).deliver_now 
         elsif (@event.previous_changes['active'] == [true, false]) && (@event.lock == true)
           flash[:event_notifications] = 'Event is completed.'
           EventMailer.event_completed(@event).deliver_now
        end
        format.html { redirect_to @event }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:podcast_url, :title, :location, :arena, :pay_per_view, :active, :start_time, :fighter_1_id, :fighter_2_id, :season_id, :lock, :fights_attributes => [:id, :winner, :method, :round, :fighter_1_odds, :fighter_2_odds])
    end
    def verify_is_admin
      (current_user.nil?) ? redirect_to(root_path) : (redirect_to(root_path) unless current_user.admin?)
    end    
end
