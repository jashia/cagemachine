class ChallengeMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.challenge_mailer.new_challenge.subject
  #
  def new_challenge(challenge)
    @challenge = challenge

    mail to: challenge.challenged.email,
    from: "The Cage Machine <cagemachine@cageagainst.com>",
    subject: "Check The Calendar, I Warn Any Challenger"


  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.challenge_mailer.existing_challenge.subject
  #
  def existing_challenge(challenge)
    @challenge = challenge
    mail to: challenge.challenger.email,
    from: "The Cage Machine <cagemachine@cageagainst.com>",
    subject: "Double Down Sometime"

  end
end
