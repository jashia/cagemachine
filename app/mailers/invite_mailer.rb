class InviteMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.invite_mailer.new_user_invite.subject
  #
  def new_user_invite(invite, token)
    @invite = invite
    @token = token
    @nextevent = Event.where(active: true).order("start_time").first
    mail  to: invite.email, 
          from: "The Cage Machine <cagemachine@cageagainst.com>", 
          subject: 'Welcome to the Machine!'
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.invite_mailer.existing_user_invite.subject
  #
  def existing_user_invite(invite)
    @invite = invite
    @nextevent = Event.where(active: true).order("start_time").first
    mail to: invite.email, 
    from: "The Cage Machine <cagemachine@cageagainst.com>", 
    subject: 'You have been invited to Join a league.'
  end
end
