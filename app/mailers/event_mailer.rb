class EventMailer < ApplicationMailer

	def new_active_event(event, user)
		@event = event
		@user = user
		mail from: "The Cage Machine <cagemachine@cageagainst.com>", 
		to: user.email, # bcc: User.all.pluck(:email),
		subject: "Once Again Its On!",
		reply_to: 'cageagainst@gmail.com'
	end
	
	def event_locked(event)
		@event = event
		mail from: "The Cage Machine <cagemachine@cageagainst.com>", 
		to: ["ja.shia@gmail.com", "bsweet@dualismgroup.com"],
		subject: "This event is now locked!",
		reply_to: 'cageagainst@gmail.com'
	end

	def event_completed(event)
		@event = event
		mail from: "The Cage Machine <cagemachine@cageagainst.com>", 
		to: ["ja.shia@gmail.com", "bsweet@dualismgroup.com"],
		subject: "What a great fight night!",
		reply_to: 'cageagainst@gmail.com'
	end

end
