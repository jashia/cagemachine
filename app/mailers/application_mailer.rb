class ApplicationMailer < ActionMailer::Base
  default from: "The Cage Machine <cagemachine@cageagainst.com>" 
  layout 'mailer'
end
