class UserMailer < ApplicationMailer

	def new_user(user)
		@user = user
		@subject = "Welcome to the Machine!"
		@cage_message = ""
	    @nextevent = Event.where(active: true).order("start_time").first

	    mail  to: user.email, 
        	  from: "The Cage Machine <cagemachine@cageagainst.com>", 
       	   	  subject: 'Welcome to the Machine!'
	end
end
