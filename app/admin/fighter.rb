ActiveAdmin.register Fighter do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :country, :first_name, :last_name, :nickname, :weightclass, :active
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

index do
	column :id
	column :display_name
	column :active
	actions
end


end
