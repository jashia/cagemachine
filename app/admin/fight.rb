ActiveAdmin.register Fight do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :event_id, :fighter_1_id, :fighter_1_odds, :fighter_2_id, :fighter_2_odds, :order, :bout_type, :five_rounds, :championship_bout, :winner, :method, :round, :fighter_1_info, :fighter_2_info, :lock
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
	index do 
		column :event
		column :order
		column :fighter_1
		column :fighter_2
		column :winner
		column :method
		column :round
		actions
	end

	form do |f|
		inputs
		actions
	end
end
