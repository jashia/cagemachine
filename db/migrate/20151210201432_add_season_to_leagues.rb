class AddSeasonToLeagues < ActiveRecord::Migration
  def change
    add_reference :leagues, :season, index: true, foreign_key: true, default: 1
  end
end
