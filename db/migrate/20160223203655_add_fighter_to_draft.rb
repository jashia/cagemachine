class AddFighterToDraft < ActiveRecord::Migration
  def change
    add_reference :drafts, :fighter, index: true, foreign_key: true
  end
end
