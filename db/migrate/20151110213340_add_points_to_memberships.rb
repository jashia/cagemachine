class AddPointsToMemberships < ActiveRecord::Migration
  def change
    add_column :memberships, :points, :integer, default: 0
  end
end
