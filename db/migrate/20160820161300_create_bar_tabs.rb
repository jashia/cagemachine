class CreateBarTabs < ActiveRecord::Migration
  def change
    create_table :bar_tabs do |t|
      t.belongs_to :challenge, index: true, foreign_key: true
      t.string :item
      t.integer :table
      t.text :status, :default => 0
      t.boolean :delivered, :default => false

      t.timestamps null: false
    end
  end
end
