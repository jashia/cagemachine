class CreateFights < ActiveRecord::Migration
  def change
    create_table :fights do |t|
      t.belongs_to :event, index: true, foreign_key: true
      t.integer :fighter_1_id
      t.integer :fighter_1_odds
      t.integer :fighter_2_id
      t.integer :fighter_2_odds
      t.integer :order
      t.string :bout_type
      t.boolean :five_rounds
      t.boolean :championship_bout
      t.string :winner
      t.string :method
      t.integer :round

      t.timestamps null: false
    end
  end
end
