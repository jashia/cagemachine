class CreateDrafts < ActiveRecord::Migration
  def change
    create_table :drafts do |t|
      t.belongs_to :event
      t.belongs_to :user, index: true, foreign_key: true
      t.integer :fighter_1_id
      t.integer :fighter_2_id
      t.integer :fighter_3_id

      t.timestamps null: false
    end
  end
end
