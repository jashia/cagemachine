class CreateFighters < ActiveRecord::Migration
  def change
    create_table :fighters do |t|
      t.string :first_name
      t.string :last_name
      t.string :nickname
      t.string :weightclass
      t.string :country
      t.boolean :active

      t.timestamps null: false
    end
  end
end
