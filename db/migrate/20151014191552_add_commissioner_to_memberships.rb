class AddCommissionerToMemberships < ActiveRecord::Migration
  def change
    add_column :memberships, :commissioner, :boolean
  end
end
