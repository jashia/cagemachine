class AddInfoToFights < ActiveRecord::Migration
  def change
    add_column :fights, :fighter_1_info, :text
    add_column :fights, :fighter_2_info, :text
  end
end
