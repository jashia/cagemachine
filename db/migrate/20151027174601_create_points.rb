class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.integer :points
      t.integer :money
      t.integer :challenges

      t.timestamps null: false
    end
  end
end
