class AddKindToLeagues < ActiveRecord::Migration
  def change
    add_column :leagues, :kind, :string
  end
end
