class CreateFlares < ActiveRecord::Migration
  def change
    create_table :flares do |t|
      t.string :title
      t.text :description
      t.string :image
      t.boolean :premium

      t.timestamps null: false
    end
  end
end
