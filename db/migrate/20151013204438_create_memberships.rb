class CreateMemberships < ActiveRecord::Migration
  def change
    create_table :memberships do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :league, index: true, foreign_key: true

      t.timestamps null: false
    end
   	add_index :memberships, [:user_id, :league_id], unique: true

  end
end
