class ChangeColumnsOnEvents < ActiveRecord::Migration
  def change
  	rename_column :events, :fighter_1, :fighter_1_id
  	rename_column :events, :fighter_2, :fighter_2_id 
  end
end
