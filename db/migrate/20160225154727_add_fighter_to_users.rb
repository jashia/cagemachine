class AddFighterToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :fighter, index: true, foreign_key: true
  end
end
