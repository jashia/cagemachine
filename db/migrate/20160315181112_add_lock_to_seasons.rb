class AddLockToSeasons < ActiveRecord::Migration
  def change
    add_column :seasons, :lock, :boolean
  end
end
