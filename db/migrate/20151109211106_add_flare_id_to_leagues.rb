class AddFlareIdToLeagues < ActiveRecord::Migration
  def change
    add_reference :leagues, :flare, index: true, foreign_key: true, default: 1
  end
end
