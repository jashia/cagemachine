class AddIndexToGames < ActiveRecord::Migration
  def change
  	add_index :games, [:user_id, :event_id], unique: true
  end
end
