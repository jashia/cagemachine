class AddLockToFights < ActiveRecord::Migration
  def change
    add_column :fights, :lock, :boolean
  end
end
