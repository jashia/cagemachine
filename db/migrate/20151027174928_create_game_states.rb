class CreateGameStates < ActiveRecord::Migration
  def change
    create_table :game_states do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :event, index: true, foreign_key: true
      t.boolean :picks_locked
      t.boolean :roster_locked

      t.timestamps null: false
    end
  end
end
