class CreatePicks < ActiveRecord::Migration
  def change
    create_table :picks do |t|
      t.belongs_to :fight, index: true, foreign_key: true
      t.belongs_to :user, index: true, foreign_key: true
      t.string :winner
      t.string :method
      t.integer :round

      t.timestamps null: false
    end
  end
end
