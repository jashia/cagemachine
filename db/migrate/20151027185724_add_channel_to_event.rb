class AddChannelToEvent < ActiveRecord::Migration
  def change
    add_column :events, :channel, :string
  end
end
