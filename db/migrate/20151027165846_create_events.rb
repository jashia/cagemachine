class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.string :location
      t.string :arena
      t.boolean :pay_per_view
      t.boolean :active
      t.datetime :start_time
      t.integer :fighter_1
      t.integer :fighter_2

      t.timestamps null: false
    end
  end
end
