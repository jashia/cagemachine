class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.belongs_to :fight, index: true, foreign_key: true
      t.string :stage
      t.integer :challenger_id
      t.integer :challenged_id

      t.timestamps null: false
    end
  end
end
