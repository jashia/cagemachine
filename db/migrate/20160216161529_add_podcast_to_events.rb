class AddPodcastToEvents < ActiveRecord::Migration
  def change
    add_column :events, :podcast_url, :string
  end
end
