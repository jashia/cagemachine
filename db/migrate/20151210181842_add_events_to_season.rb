class AddEventsToSeason < ActiveRecord::Migration
  def change
    add_reference :events, :season, index: true, foreign_key: true
  end
end
