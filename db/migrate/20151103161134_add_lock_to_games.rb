class AddLockToGames < ActiveRecord::Migration
  def change
    add_column :games, :lock, :boolean
  end
end
