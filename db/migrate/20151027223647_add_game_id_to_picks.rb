class AddGameIdToPicks < ActiveRecord::Migration
  def change
    add_reference :picks, :game, index: true, foreign_key: true
  end
end
