class AddLegacyDataToFighters < ActiveRecord::Migration
  def change
    add_column :fighters, :legacy_id, :integer
    add_column :fighters, :legacy_link, :string
    add_column :fighters, :legacy_wins, :integer
    add_column :fighters, :legacy_losses, :integer
    add_column :fighters, :legacy_draws, :integer
  end
end
