class AddRosterToDrafts < ActiveRecord::Migration
  def change
    add_reference :drafts, :roster, index: true, foreign_key: true
  end
end
