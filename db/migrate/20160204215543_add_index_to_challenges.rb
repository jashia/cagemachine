class AddIndexToChallenges < ActiveRecord::Migration
  def change
  	add_index :challenges, [:challenger_id, :challenged_id, :fight_id], unique: true, :name => 'unique_challenges_index'
  end
end
