class AddWinnerToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :winner, :string
  end
end
