class CreateRosters < ActiveRecord::Migration
  def change
    create_table :rosters do |t|
      t.belongs_to :membership, index: true, foreign_key: true
      t.string :status
      t.boolean :locked

      t.timestamps null: false
    end
  end
end
