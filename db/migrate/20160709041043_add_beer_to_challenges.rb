class AddBeerToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :beer, :boolean
  end
end
