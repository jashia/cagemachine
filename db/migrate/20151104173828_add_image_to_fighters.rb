class AddImageToFighters < ActiveRecord::Migration
  def change
    add_column :fighters, :image, :string
  end
end
