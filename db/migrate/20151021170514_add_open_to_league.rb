class AddOpenToLeague < ActiveRecord::Migration
  def change
    add_column :leagues, :open, :boolean
  end
end
