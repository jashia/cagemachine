class AddLockToEvents < ActiveRecord::Migration
  def change
    add_column :events, :lock, :boolean
  end
end
