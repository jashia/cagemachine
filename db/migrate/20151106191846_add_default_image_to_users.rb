class AddDefaultImageToUsers < ActiveRecord::Migration
  def change
  	change_column :users, :flare, :string, :default => 'placeholder.png'
  end
end
