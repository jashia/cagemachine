class AddOwnerToLeague < ActiveRecord::Migration
  def change
    add_column :leagues, :owner_id, :integer, index: true
  end
end
