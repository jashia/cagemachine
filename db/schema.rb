# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160820173927) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true

  create_table "bar_tabs", force: :cascade do |t|
    t.integer  "challenge_id"
    t.string   "item"
    t.integer  "table"
    t.text     "status",       default: "0"
    t.boolean  "delivered",    default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "bar_tabs", ["challenge_id"], name: "index_bar_tabs_on_challenge_id"

  create_table "challenges", force: :cascade do |t|
    t.integer  "fight_id"
    t.string   "stage"
    t.integer  "challenger_id"
    t.integer  "challenged_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.boolean  "beer"
    t.string   "winner"
  end

  add_index "challenges", ["challenger_id", "challenged_id", "fight_id"], name: "unique_challenges_index", unique: true
  add_index "challenges", ["fight_id"], name: "index_challenges_on_fight_id"

  create_table "drafts", force: :cascade do |t|
    t.integer  "event_id"
    t.integer  "user_id"
    t.integer  "fighter_1_id"
    t.integer  "fighter_2_id"
    t.integer  "fighter_3_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "roster_id"
    t.integer  "fighter_id"
  end

  add_index "drafts", ["fighter_id"], name: "index_drafts_on_fighter_id"
  add_index "drafts", ["roster_id"], name: "index_drafts_on_roster_id"
  add_index "drafts", ["user_id"], name: "index_drafts_on_user_id"

  create_table "events", force: :cascade do |t|
    t.string   "title"
    t.string   "location"
    t.string   "arena"
    t.boolean  "pay_per_view"
    t.boolean  "active"
    t.datetime "start_time"
    t.integer  "fighter_1_id"
    t.integer  "fighter_2_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "channel"
    t.integer  "season_id"
    t.string   "podcast_url"
    t.boolean  "lock"
  end

  add_index "events", ["season_id"], name: "index_events_on_season_id"

  create_table "fighters", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "nickname"
    t.string   "weightclass"
    t.string   "country"
    t.boolean  "active"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "image"
    t.integer  "legacy_id"
    t.string   "legacy_link"
    t.integer  "legacy_wins"
    t.integer  "legacy_losses"
    t.integer  "legacy_draws"
  end

  create_table "fights", force: :cascade do |t|
    t.integer  "event_id"
    t.integer  "fighter_1_id"
    t.integer  "fighter_1_odds"
    t.integer  "fighter_2_id"
    t.integer  "fighter_2_odds"
    t.integer  "order"
    t.string   "bout_type"
    t.boolean  "five_rounds"
    t.boolean  "championship_bout"
    t.string   "winner"
    t.string   "method"
    t.integer  "round"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.text     "fighter_1_info"
    t.text     "fighter_2_info"
    t.boolean  "lock"
  end

  add_index "fights", ["event_id"], name: "index_fights_on_event_id"

  create_table "flares", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "image"
    t.boolean  "premium"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "game_states", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.boolean  "picks_locked"
    t.boolean  "roster_locked"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "game_states", ["event_id"], name: "index_game_states_on_event_id"
  add_index "game_states", ["user_id"], name: "index_game_states_on_user_id"

  create_table "games", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.string   "game_type"
    t.integer  "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "lock"
  end

  add_index "games", ["event_id"], name: "index_games_on_event_id"
  add_index "games", ["user_id", "event_id"], name: "index_games_on_user_id_and_event_id", unique: true
  add_index "games", ["user_id"], name: "index_games_on_user_id"

  create_table "invites", force: :cascade do |t|
    t.string   "email"
    t.integer  "league_id"
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.string   "token"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invites", ["league_id"], name: "index_invites_on_league_id"

  create_table "leagues", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.boolean  "open"
    t.integer  "owner_id"
    t.integer  "flare_id",   default: 1
    t.integer  "season_id",  default: 1
    t.string   "kind"
  end

  add_index "leagues", ["flare_id"], name: "index_leagues_on_flare_id"
  add_index "leagues", ["season_id"], name: "index_leagues_on_season_id"

  create_table "memberships", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "league_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.boolean  "commissioner"
    t.string   "email"
    t.integer  "points",       default: 0
  end

  add_index "memberships", ["league_id"], name: "index_memberships_on_league_id"
  add_index "memberships", ["user_id", "league_id"], name: "index_memberships_on_user_id_and_league_id", unique: true
  add_index "memberships", ["user_id"], name: "index_memberships_on_user_id"

  create_table "picks", force: :cascade do |t|
    t.integer  "fight_id"
    t.integer  "user_id"
    t.string   "winner"
    t.string   "method"
    t.integer  "round"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "game_id"
  end

  add_index "picks", ["fight_id"], name: "index_picks_on_fight_id"
  add_index "picks", ["game_id"], name: "index_picks_on_game_id"
  add_index "picks", ["user_id"], name: "index_picks_on_user_id"

  create_table "points", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "points"
    t.integer  "money"
    t.integer  "challenges"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "points", ["user_id"], name: "index_points_on_user_id"

  create_table "rosters", force: :cascade do |t|
    t.integer  "membership_id"
    t.string   "status"
    t.boolean  "locked"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "rosters", ["membership_id"], name: "index_rosters_on_membership_id"

  create_table "seasons", force: :cascade do |t|
    t.string   "name"
    t.date     "start"
    t.date     "end"
    t.boolean  "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "lock"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",                null: false
    t.string   "encrypted_password",     default: "",                null: false
    t.boolean  "admin",                  default: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,                 null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "username"
    t.integer  "points",                 default: 0
    t.string   "flare",                  default: "placeholder.png"
    t.boolean  "terms"
    t.integer  "coins",                  default: 10
    t.integer  "fighter_id"
    t.string   "first_name"
    t.string   "last_name"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["fighter_id"], name: "index_users_on_fighter_id"
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
