require 'json'

fighters_file = File.read("db/docs_to_import/fighters.json")
fighters = JSON.parse(fighters_file)
fighters.each do |fighter|
	first_name = fighter["first_name"]
	last_name = fighter["last_name"]
	legacy_id = fighter["id"]
	full_name = first_name + " " + last_name

	if Fighter.where("first_name = ? and last_name = ?", first_name, last_name).exists?
		"EXISTS ALREADY"
	else
		puts full_name, legacy_id
	end
end