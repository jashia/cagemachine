CarrierWave.configure do |config|
	if Rails.env.development? || Rails.env.test?
    	config.storage = :file
  	else
	  config.fog_credentials = {
	    provider:              'AWS',                        # required
	    aws_access_key_id:     ENV["S3_ACCESS_KEY_ID"],                        # required
	    aws_secret_access_key: ENV["S3_SECRET_ACCESS_KEY"],
	    region: 				'us-west-1'                        # required

	  }
      config.storage = :fog
	  config.fog_directory  = ENV["AWS_S3_BUCKET"]                          # required
	  config.cache_dir = "#{Rails.root}/tmp/uploads"
	end
end