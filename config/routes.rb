Rails.application.routes.draw do
  resources :bar_tabs
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :flares
  resources :games do
    collection do
      patch :submit_picks, :action => :submit_picks
    end
  end
  resources :game_states
  resources :points
  resources :rosters do
    resources :drafts
  end
  resources :picks
  resources :challenges
  resources :fights
  resources :fighters, shallow: true do 
    get 'fighter_info', to: 'fighters#fighter_info'
  end
  resources :events
  resources :invites
  resources :seasons
  devise_for :users, :controllers => { :registrations => "users/registrations" }
  resources :users
  resources :leagues 
  resources :memberships
  
  root 'static_pages#home'

  get 'static_pages/home'

  get 'about', to: 'static_pages#about'
  get 'terms', to: 'static_pages#terms'
  get 'rules', to: 'static_pages#rules'

  get 'static_pages/help'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
