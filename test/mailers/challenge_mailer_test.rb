require 'test_helper'

class ChallengeMailerTest < ActionMailer::TestCase
  test "new_challenge" do
    mail = ChallengeMailer.new_challenge
    assert_equal "New challenge", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "existing_challenge" do
    mail = ChallengeMailer.existing_challenge
    assert_equal "Existing challenge", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
