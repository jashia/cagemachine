# Preview all emails at http://localhost:3000/rails/mailers/challenge_mailer
class ChallengeMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/challenge_mailer/new_challenge
  def new_challenge
    ChallengeMailer.new_challenge
  end

  # Preview this email at http://localhost:3000/rails/mailers/challenge_mailer/existing_challenge
  def existing_challenge
    ChallengeMailer.existing_challenge
  end

end
