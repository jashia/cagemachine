require 'test_helper'

class FightsControllerTest < ActionController::TestCase
  setup do
    @fight = fights(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fights)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fight" do
    assert_difference('Fight.count') do
      post :create, fight: { bout_type: @fight.bout_type, championship_bout: @fight.championship_bout, event_id: @fight.event_id, fighter_1_id: @fight.fighter_1_id, fighter_1_odds: @fight.fighter_1_odds, fighter_2_id: @fight.fighter_2_id, fighter_2_odds: @fight.fighter_2_odds, five_rounds: @fight.five_rounds, method: @fight.method, order: @fight.order, round: @fight.round, winner: @fight.winner }
    end

    assert_redirected_to fight_path(assigns(:fight))
  end

  test "should show fight" do
    get :show, id: @fight
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fight
    assert_response :success
  end

  test "should update fight" do
    patch :update, id: @fight, fight: { bout_type: @fight.bout_type, championship_bout: @fight.championship_bout, event_id: @fight.event_id, fighter_1_id: @fight.fighter_1_id, fighter_1_odds: @fight.fighter_1_odds, fighter_2_id: @fight.fighter_2_id, fighter_2_odds: @fight.fighter_2_odds, five_rounds: @fight.five_rounds, method: @fight.method, order: @fight.order, round: @fight.round, winner: @fight.winner }
    assert_redirected_to fight_path(assigns(:fight))
  end

  test "should destroy fight" do
    assert_difference('Fight.count', -1) do
      delete :destroy, id: @fight
    end

    assert_redirected_to fights_path
  end
end
