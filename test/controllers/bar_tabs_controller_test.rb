require 'test_helper'

class BarTabsControllerTest < ActionController::TestCase
  setup do
    @bar_tab = bar_tabs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bar_tabs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bar_tab" do
    assert_difference('BarTab.count') do
      post :create, bar_tab: { delivered: @bar_tab.delivered, item: @bar_tab.item, status: @bar_tab.status, table: @bar_tab.table, user_id: @bar_tab.user_id }
    end

    assert_redirected_to bar_tab_path(assigns(:bar_tab))
  end

  test "should show bar_tab" do
    get :show, id: @bar_tab
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bar_tab
    assert_response :success
  end

  test "should update bar_tab" do
    patch :update, id: @bar_tab, bar_tab: { delivered: @bar_tab.delivered, item: @bar_tab.item, status: @bar_tab.status, table: @bar_tab.table, user_id: @bar_tab.user_id }
    assert_redirected_to bar_tab_path(assigns(:bar_tab))
  end

  test "should destroy bar_tab" do
    assert_difference('BarTab.count', -1) do
      delete :destroy, id: @bar_tab
    end

    assert_redirected_to bar_tabs_path
  end
end
