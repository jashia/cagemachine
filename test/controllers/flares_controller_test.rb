require 'test_helper'

class FlaresControllerTest < ActionController::TestCase
  setup do
    @flare = flares(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:flares)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create flare" do
    assert_difference('Flare.count') do
      post :create, flare: { description: @flare.description, image: @flare.image, premium: @flare.premium, title: @flare.title }
    end

    assert_redirected_to flare_path(assigns(:flare))
  end

  test "should show flare" do
    get :show, id: @flare
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @flare
    assert_response :success
  end

  test "should update flare" do
    patch :update, id: @flare, flare: { description: @flare.description, image: @flare.image, premium: @flare.premium, title: @flare.title }
    assert_redirected_to flare_path(assigns(:flare))
  end

  test "should destroy flare" do
    assert_difference('Flare.count', -1) do
      delete :destroy, id: @flare
    end

    assert_redirected_to flares_path
  end
end
